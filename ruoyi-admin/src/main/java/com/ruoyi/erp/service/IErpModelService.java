package com.ruoyi.erp.service;

import java.util.List;
import com.ruoyi.erp.domain.ErpModel;

/**
 * 生产型号Service接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface IErpModelService 
{
    /**
     * 查询生产型号
     * 
     * @param id 生产型号ID
     * @return 生产型号
     */
    public ErpModel selectErpModelById(String id);

    /**
     * 查询生产型号列表
     * 
     * @param erpModel 生产型号
     * @return 生产型号集合
     */
    public List<ErpModel> selectErpModelList(ErpModel erpModel);

    /**
     * 新增生产型号
     * 
     * @param erpModel 生产型号
     * @return 结果
     */
    public int insertErpModel(ErpModel erpModel);

    /**
     * 修改生产型号
     * 
     * @param erpModel 生产型号
     * @return 结果
     */
    public int updateErpModel(ErpModel erpModel);

    /**
     * 批量删除生产型号
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpModelByIds(String ids);

    /**
     * 删除生产型号信息
     * 
     * @param id 生产型号ID
     * @return 结果
     */
    public int deleteErpModelById(String id);
}
