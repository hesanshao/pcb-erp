package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpClassifyModeAttributeRtMapper;
import com.ruoyi.erp.domain.ErpClassifyModeAttributeRt;
import com.ruoyi.erp.service.IErpClassifyModeAttributeRtService;
import com.ruoyi.common.core.text.Convert;

/**
 * 分类型号属性关联Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpClassifyModeAttributeRtServiceImpl implements IErpClassifyModeAttributeRtService 
{
    @Autowired
    private ErpClassifyModeAttributeRtMapper erpClassifyModeAttributeRtMapper;

    /**
     * 查询分类型号属性关联
     * 
     * @param id 分类型号属性关联ID
     * @return 分类型号属性关联
     */
    @Override
    public ErpClassifyModeAttributeRt selectErpClassifyModeAttributeRtById(String id)
    {
        return erpClassifyModeAttributeRtMapper.selectErpClassifyModeAttributeRtById(id);
    }

    /**
     * 查询分类型号属性关联列表
     * 
     * @param erpClassifyModeAttributeRt 分类型号属性关联
     * @return 分类型号属性关联
     */
    @Override
    public List<ErpClassifyModeAttributeRt> selectErpClassifyModeAttributeRtList(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt)
    {
        return erpClassifyModeAttributeRtMapper.selectErpClassifyModeAttributeRtList(erpClassifyModeAttributeRt);
    }

    /**
     * 新增分类型号属性关联
     * 
     * @param erpClassifyModeAttributeRt 分类型号属性关联
     * @return 结果
     */
    @Override
    public int insertErpClassifyModeAttributeRt(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt)
    {
        erpClassifyModeAttributeRt.setId(IdUtils.fastSimpleUUID());
        erpClassifyModeAttributeRt.setCreateTime(DateUtils.getNowDate());
        return erpClassifyModeAttributeRtMapper.insertErpClassifyModeAttributeRt(erpClassifyModeAttributeRt);
    }

    /**
     * 修改分类型号属性关联
     * 
     * @param erpClassifyModeAttributeRt 分类型号属性关联
     * @return 结果
     */
    @Override
    public int updateErpClassifyModeAttributeRt(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt)
    {
        erpClassifyModeAttributeRt.setUpdateTime(DateUtils.getNowDate());
        return erpClassifyModeAttributeRtMapper.updateErpClassifyModeAttributeRt(erpClassifyModeAttributeRt);
    }

    /**
     * 删除分类型号属性关联对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpClassifyModeAttributeRtByIds(String ids)
    {
        return erpClassifyModeAttributeRtMapper.deleteErpClassifyModeAttributeRtByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除分类型号属性关联信息
     * 
     * @param id 分类型号属性关联ID
     * @return 结果
     */
    @Override
    public int deleteErpClassifyModeAttributeRtById(String id)
    {
        return erpClassifyModeAttributeRtMapper.deleteErpClassifyModeAttributeRtById(id);
    }
}
