package com.ruoyi.erp.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生产批次对象 erp_batch_info
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpBatchInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 批次号 */
    @Excel(name = "批次号")
    private String batchNo;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNo;

    /** 销售员ID */
    @Excel(name = "销售员ID")
    private String salesmanNo;

    /** 生产型号 */
    @Excel(name = "生产型号")
    private String modelNo;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private String customerNo;

    /** 批次状态 */
    @Excel(name = "批次状态")
    private String state;

    /** 生产优先级 */
    @Excel(name = "生产优先级")
    private Long batchPriority;

    /** 长 */
    @Excel(name = "长")
    private Long length;

    /** 宽 */
    @Excel(name = "宽")
    private Long wide;

    /** PNL长 */
    @Excel(name = "PNL长")
    private Long pnlLength;

    /** PNL宽 */
    @Excel(name = "PNL宽")
    private Long pnlWide;

    /** PCS数 */
    @Excel(name = "PCS数")
    private Long pcsNum;

    /** 连片数量 */
    @Excel(name = "连片数量")
    private Long continuousNum;

    /** 连片数量 */
    @Excel(name = "连片数量")
    private Long deliveryNum;

    /** 投料数量 */
    @Excel(name = "投料数量")
    private Long feedNum;

    /** 开料日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开料日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date cuttingTime;

    /** 当前工序状态 */
    @Excel(name = "当前工序状态")
    private String processState;

    /** PNL总数 */
    @Excel(name = "PNL总数")
    private Long pnlTotalNum;

    /** 欠料 */
    @Excel(name = "欠料")
    private Long qlNum;

    /** 开料 */
    @Excel(name = "开料")
    private Long klNum;

    /** 压合 */
    @Excel(name = "压合")
    private Long yhNum;

    /** 钻孔 */
    @Excel(name = "钻孔")
    private Long zkNum;

    /** 导电胶 */
    @Excel(name = "导电胶")
    private Long ddjNum;

    /** 线路 */
    @Excel(name = "线路")
    private Long xlNum;

    /** 线检 */
    @Excel(name = "线检")
    private Long xjNum;

    /** 图电 */
    @Excel(name = "图电")
    private Long tdNum;

    /** 蚀检 */
    @Excel(name = "蚀检")
    private Long sjNum;

    /** 中测 */
    @Excel(name = "中测")
    private Long zcNum;

    /** 阻焊 */
    @Excel(name = "阻焊")
    private Long zhNum;

    /** 阻检 */
    @Excel(name = "阻检")
    private Long zjNum;

    /** 字符文字 */
    @Excel(name = "字符文字")
    private Long zfwzNum;

    /** 表面 */
    @Excel(name = "表面")
    private Long bmclNum;

    /** 二钻 */
    @Excel(name = "二钻")
    private Long ezNum;

    /** 大板飞针 */
    @Excel(name = "大板飞针")
    private Long dbfzNum;

    /** 成型外形 */
    @Excel(name = "成型外形")
    private Long cxwxNum;

    /** 测试 */
    @Excel(name = "测试")
    private Long testNum;

    /** FQC */
    @Excel(name = "FQC")
    private Long fqcNum;

    /** 已入库（包装） */
    private Long yrkNum;

    /** 已出库 */
    private Long yckNum;

    /** 已完结 */
    private Long ywjNum;

    /** 累计入库 */
    @Excel(name = "累计入库")
    private Long rkTotal;

    /** 订单面积 */
    @Excel(name = "订单面积")
    private BigDecimal orderArea;

    /** 生产面积 */
    @Excel(name = "生产面积")
    private BigDecimal prdArea;

    /** 在线PNL */
    @Excel(name = "在线PNL")
    private Long linePnl;

    /** 在线面积 */
    @Excel(name = "在线面积")
    private BigDecimal lineArea;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setBatchNo(String batchNo) 
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo() 
    {
        return batchNo;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setSalesmanNo(String salesmanNo) 
    {
        this.salesmanNo = salesmanNo;
    }

    public String getSalesmanNo() 
    {
        return salesmanNo;
    }
    public void setModelNo(String modelNo) 
    {
        this.modelNo = modelNo;
    }

    public String getModelNo() 
    {
        return modelNo;
    }
    public void setCustomerNo(String customerNo) 
    {
        this.customerNo = customerNo;
    }

    public String getCustomerNo() 
    {
        return customerNo;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setBatchPriority(Long batchPriority) 
    {
        this.batchPriority = batchPriority;
    }

    public Long getBatchPriority() 
    {
        return batchPriority;
    }
    public void setLength(Long length) 
    {
        this.length = length;
    }

    public Long getLength() 
    {
        return length;
    }
    public void setWide(Long wide) 
    {
        this.wide = wide;
    }

    public Long getWide() 
    {
        return wide;
    }
    public void setPnlLength(Long pnlLength) 
    {
        this.pnlLength = pnlLength;
    }

    public Long getPnlLength() 
    {
        return pnlLength;
    }
    public void setPnlWide(Long pnlWide) 
    {
        this.pnlWide = pnlWide;
    }

    public Long getPnlWide() 
    {
        return pnlWide;
    }
    public void setPcsNum(Long pcsNum) 
    {
        this.pcsNum = pcsNum;
    }

    public Long getPcsNum() 
    {
        return pcsNum;
    }
    public void setContinuousNum(Long continuousNum) 
    {
        this.continuousNum = continuousNum;
    }

    public Long getContinuousNum() 
    {
        return continuousNum;
    }
    public void setDeliveryNum(Long deliveryNum) 
    {
        this.deliveryNum = deliveryNum;
    }

    public Long getDeliveryNum() 
    {
        return deliveryNum;
    }
    public void setFeedNum(Long feedNum) 
    {
        this.feedNum = feedNum;
    }

    public Long getFeedNum() 
    {
        return feedNum;
    }
    public void setCuttingTime(Date cuttingTime) 
    {
        this.cuttingTime = cuttingTime;
    }

    public Date getCuttingTime() 
    {
        return cuttingTime;
    }
    public void setProcessState(String processState)
    {
        this.processState = processState;
    }

    public String getProcessState()
    {
        return processState;
    }
    public void setPnlTotalNum(Long pnlTotalNum)
    {
        this.pnlTotalNum = pnlTotalNum;
    }

    public Long getPnlTotalNum() 
    {
        return pnlTotalNum;
    }
    public void setQlNum(Long qlNum) 
    {
        this.qlNum = qlNum;
    }

    public Long getQlNum() 
    {
        return qlNum;
    }
    public void setKlNum(Long klNum) 
    {
        this.klNum = klNum;
    }

    public Long getKlNum() 
    {
        return klNum;
    }
    public void setYhNum(Long yhNum) 
    {
        this.yhNum = yhNum;
    }

    public Long getYhNum() 
    {
        return yhNum;
    }
    public void setZkNum(Long zkNum) 
    {
        this.zkNum = zkNum;
    }

    public Long getZkNum() 
    {
        return zkNum;
    }
    public void setDdjNum(Long ddjNum) 
    {
        this.ddjNum = ddjNum;
    }

    public Long getDdjNum() 
    {
        return ddjNum;
    }
    public void setXlNum(Long xlNum) 
    {
        this.xlNum = xlNum;
    }

    public Long getXlNum() 
    {
        return xlNum;
    }
    public void setXjNum(Long xjNum) 
    {
        this.xjNum = xjNum;
    }

    public Long getXjNum() 
    {
        return xjNum;
    }
    public void setTdNum(Long tdNum) 
    {
        this.tdNum = tdNum;
    }

    public Long getTdNum() 
    {
        return tdNum;
    }
    public void setSjNum(Long sjNum) 
    {
        this.sjNum = sjNum;
    }

    public Long getSjNum() 
    {
        return sjNum;
    }
    public void setZcNum(Long zcNum) 
    {
        this.zcNum = zcNum;
    }

    public Long getZcNum() 
    {
        return zcNum;
    }
    public void setZhNum(Long zhNum) 
    {
        this.zhNum = zhNum;
    }

    public Long getZhNum() 
    {
        return zhNum;
    }
    public void setZjNum(Long zjNum) 
    {
        this.zjNum = zjNum;
    }

    public Long getZjNum() 
    {
        return zjNum;
    }
    public void setZfwzNum(Long zfwzNum) 
    {
        this.zfwzNum = zfwzNum;
    }

    public Long getZfwzNum() 
    {
        return zfwzNum;
    }
    public void setBmclNum(Long bmclNum) 
    {
        this.bmclNum = bmclNum;
    }

    public Long getBmclNum() 
    {
        return bmclNum;
    }
    public void setEzNum(Long ezNum) 
    {
        this.ezNum = ezNum;
    }

    public Long getEzNum() 
    {
        return ezNum;
    }
    public void setDbfzNum(Long dbfzNum) 
    {
        this.dbfzNum = dbfzNum;
    }

    public Long getDbfzNum() 
    {
        return dbfzNum;
    }
    public void setCxwxNum(Long cxwxNum) 
    {
        this.cxwxNum = cxwxNum;
    }

    public Long getCxwxNum() 
    {
        return cxwxNum;
    }
    public void setTestNum(Long testNum) 
    {
        this.testNum = testNum;
    }

    public Long getTestNum() 
    {
        return testNum;
    }
    public void setFqcNum(Long fqcNum) 
    {
        this.fqcNum = fqcNum;
    }

    public Long getFqcNum() 
    {
        return fqcNum;
    }
    public void setYrkNum(Long yrkNum) 
    {
        this.yrkNum = yrkNum;
    }

    public Long getYrkNum() 
    {
        return yrkNum;
    }
    public void setYckNum(Long yckNum) 
    {
        this.yckNum = yckNum;
    }

    public Long getYckNum() 
    {
        return yckNum;
    }
    public void setYwjNum(Long ywjNum) 
    {
        this.ywjNum = ywjNum;
    }

    public Long getYwjNum() 
    {
        return ywjNum;
    }
    public void setRkTotal(Long rkTotal) 
    {
        this.rkTotal = rkTotal;
    }

    public Long getRkTotal() 
    {
        return rkTotal;
    }
    public void setOrderArea(BigDecimal orderArea) 
    {
        this.orderArea = orderArea;
    }

    public BigDecimal getOrderArea() 
    {
        return orderArea;
    }
    public void setPrdArea(BigDecimal prdArea) 
    {
        this.prdArea = prdArea;
    }

    public BigDecimal getPrdArea() 
    {
        return prdArea;
    }
    public void setLinePnl(Long linePnl) 
    {
        this.linePnl = linePnl;
    }

    public Long getLinePnl() 
    {
        return linePnl;
    }
    public void setLineArea(BigDecimal lineArea) 
    {
        this.lineArea = lineArea;
    }

    public BigDecimal getLineArea() 
    {
        return lineArea;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("batchNo", getBatchNo())
            .append("orderNo", getOrderNo())
            .append("salesmanNo", getSalesmanNo())
            .append("modelNo", getModelNo())
            .append("customerNo", getCustomerNo())
            .append("state", getState())
            .append("batchPriority", getBatchPriority())
            .append("length", getLength())
            .append("wide", getWide())
            .append("pnlLength", getPnlLength())
            .append("pnlWide", getPnlWide())
            .append("pcsNum", getPcsNum())
            .append("continuousNum", getContinuousNum())
            .append("deliveryNum", getDeliveryNum())
            .append("feedNum", getFeedNum())
            .append("cuttingTime", getCuttingTime())
            .append("processState", getProcessState())
            .append("pnlTotalNum", getPnlTotalNum())
            .append("qlNum", getQlNum())
            .append("klNum", getKlNum())
            .append("yhNum", getYhNum())
            .append("zkNum", getZkNum())
            .append("ddjNum", getDdjNum())
            .append("xlNum", getXlNum())
            .append("xjNum", getXjNum())
            .append("tdNum", getTdNum())
            .append("sjNum", getSjNum())
            .append("zcNum", getZcNum())
            .append("zhNum", getZhNum())
            .append("zjNum", getZjNum())
            .append("zfwzNum", getZfwzNum())
            .append("bmclNum", getBmclNum())
            .append("ezNum", getEzNum())
            .append("dbfzNum", getDbfzNum())
            .append("cxwxNum", getCxwxNum())
            .append("testNum", getTestNum())
            .append("fqcNum", getFqcNum())
            .append("yrkNum", getYrkNum())
            .append("yckNum", getYckNum())
            .append("ywjNum", getYwjNum())
            .append("rkTotal", getRkTotal())
            .append("orderArea", getOrderArea())
            .append("prdArea", getPrdArea())
            .append("linePnl", getLinePnl())
            .append("lineArea", getLineArea())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
