package com.ruoyi.erp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品库存对象 erp_stock
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpStock extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 生产型号 */
    @Excel(name = "生产型号")
    private String modeNo;

    /** 批次号 */
    @Excel(name = "批次号")
    private String batchNo;

    /** 库存量 */
    @Excel(name = "库存量")
    private Long stockTotal;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setModeNo(String modeNo) 
    {
        this.modeNo = modeNo;
    }

    public String getModeNo() 
    {
        return modeNo;
    }
    public void setBatchNo(String batchNo) 
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo() 
    {
        return batchNo;
    }
    public void setStockTotal(Long stockTotal) 
    {
        this.stockTotal = stockTotal;
    }

    public Long getStockTotal() 
    {
        return stockTotal;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("modeNo", getModeNo())
            .append("batchNo", getBatchNo())
            .append("stockTotal", getStockTotal())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
